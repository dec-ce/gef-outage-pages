"use strict"

// Load required libraries
var gulp            = require("gulp"),
    runSequence     = require("run-sequence"),
    argv            = require("yargs").argv,
    pug             = require("gulp-pug"),
    plugins         = require("gulp-load-plugins")(),
    browserSync     = require("browser-sync").create(),
    errorHandler    = function (error) {
      console.log(error.toString())
    }

// Allow use of console.log in nodejs files
// console.log = function(d) {
//   process.stdout.write(d + '\n')
// }

// Define options and paths
var siteName = "GEF Outage Pages",
    options = {
      siteName:               siteName,
      compress:               argv.compress || false,
      uncss:                  argv.uncss || false,
      port:                   argv.port || 3000,
      distribute:             false,
      paths: {
        bower:                "bower_components/",
        gef:                  "bower_components/GEF/",
        dev: {
          root:               "src/",
          templates:          "src/*.pug",
          assets: {
            root:             "src/assets/",
            pug:             "src/assets/pug/**/*.pug",
            sass:             "src/assets/sass/**/*.{scss,sass}",
            js:               "src/assets/js/**/*.{js,es6,es7,es}",
            jsConfig:         "src/assets/js/config/",
            fonts:            "src/assets/fonts/**/*.{eot,svg,ttf,woff,woff2,otf}",
            images:           "src/assets/images/**/*.{gif,jpg,svg,png}"
          }
        },
        dist: {
          root:               "dist/",
          html:               "dist/*.html",
          assets: {
            root:             "dist/assets/",
            css:              "dist/assets/css/",
            js:               "dist/assets/js/",
            fonts:            "dist/assets/fonts/",
            images:           "dist/assets/images/",
            matrixImages:     "dist/assets/css/mysource_files/",
          }
        }
      },
      update: {
        source:               "https://bitbucket.org/dec-ce/gef-generator-master.git",
        developmentBranch:    "develop",
        tagBranch:            "tag-merge"
      },
      deployment: {
        source:               "./dist/**/*.*",
        environment:          undefined,
        repository: {
          root:               "../dec-ce.bitbucket.org/",
          deploymentLocation: "../dec-ce.bitbucket.org/" + siteName.replace(/\s+/g, "-").toLowerCase() + "/"
        },
        args: {
          args:               "",
          cwd:                "../dec-ce.bitbucket.org/",
          quiet:              false
        },
        uncss: {
          html: [
            "dist/index.html",
            "dist/scheduled-outage.html",
            "dist/unscheduled-outage.html"
          ],
          ignore: [
            /(\.gef-tabs.+)/,
            /(\.gef-tag-list.*)/,
            /(.+\.active.*)/,
            /(.+\.uk-active.*)/,
            /(.+\[aria-expanded.*)/,
            /(.+\[aria-hidden.*)/,
            /(.+iframe.*)/,
            /(.+placeholder.*)/,
            /(\.gef-ie9.+)/,
            /(\.gef-btt.+)/,
            /(\.gef-pagination.+)/,
            /(\.call-out)/,
            /(\.gef-linkgroup-list.+)/,
            /(\.gef-tile-list.+)/,
            /(\.gef-search.+)/,
            /(\.gef-external-link)/,
            /(\.gef-pseudo-link)/,
            /\[ng:cloak\]/,
            /\[ng-cloak\]/,
            /\[data-ng-cloak\]/,
            /\[x-ng-cloak\]/,
            /(\.ng-cloak)/,
            /(\.x-ng-cloak)/
          ]
        }
      }
    }

// ------------------------ //
//    TEMPLATE TASKS        //
// ------------------------ //

// Delete everything in the "build" folder
gulp.task("clean", require("./tasks/clean")(gulp, plugins, options))

// Get git revision number to append to js and css file requests
gulp.task("revision", require("./tasks/revision")(gulp, plugins, options))

// Copy libraries from bower_components/
gulp.task("copy", require("./tasks/copy")(gulp, plugins, options, errorHandler, argv))

// Copy fonts found in the project's source files
gulp.task("fonts", require("./tasks/fonts")(gulp, plugins, options, errorHandler))

// Copy images (svg, gif, png, jpg) found in the projects source files
gulp.task("images", require("./tasks/images")(gulp, plugins, options, errorHandler, argv))

// Compile CSS (SASS)
gulp.task("compile-css", require("./tasks/compile-css")(gulp, plugins, options, errorHandler, argv, browserSync))

// pug templating engine
gulp.task("pug-templates", ["revision"], require("./tasks/pug-templates")(gulp, plugins, options, errorHandler, pug))
gulp.task("pug-snippets", ["revision"], require("./tasks/pug-snippets")(gulp, plugins, options, errorHandler, pug))

// convert ECMAScript 6 & 7 code to ECMAScript 5 code
gulp.task("babel", require("./tasks/babel")(gulp, plugins, options, errorHandler))

// Web server and livereload
gulp.task("browser-sync", require("./tasks/browser-sync")(gulp, browserSync, options))

// ------------------------ //
//    UPDATE TASKS          //
// ------------------------ //

// Update the repo to match GEF Generator Master (https://bitbucket.org/dec-ce/gef-generator-master.git)
require("./tasks/update-gef")(gulp, plugins, options)

// ------------------------ //
//    DEVELOPMENT TASKS     //
// ------------------------ //

// Build Copy - All the required copy tasks in order
gulp.task("build-copy", function(cb) {

  return runSequence("clean", ["copy", "images", "fonts"], cb)
})

// Build Task - Compiles all template and code from the src into the dist folder
gulp.task("build", function(cb) {

  return runSequence("build-copy", ["pug-templates", "compile-css", "babel"], cb)
})

// Start task - runs Browser Sync so things can be served
gulp.task("start", ["build"], function() {
  return gulp.start("browser-sync")
})


// Watch task. Uses BrowserSync to provide a local development web server with livereload capability e.g.
//
// $ gulp watch
//
// You can optionally pass the "compress" CLI parameter to serve compressed assets e.g.
//
// $ gulp watch --compress
//
gulp.task("watch", ["start"], function() {
  gulp.watch(options.paths.dev.assets.sass, ["compile-css"])
  gulp.watch(options.paths.dev.assets.js, ["babel"])
  gulp.watch(options.paths.dev.assets.fonts, ["fonts"])
  gulp.watch(options.paths.dev.assets.images, ["images"])
  gulp.watch(options.paths.dev.templates, ["pug-templates"])
  gulp.watch(options.paths.dev.assets.pug, ["pug-snippets"])

  // Watch for JS and HTML
  gulp.watch([options.paths.dist.assets.js + "**/*.js", options.paths.dist.html], browserSync.reload)
})

// ------------------------ //
//    DISTRIBUTE TASKS      //
// ------------------------ //

// Set the necessary options for distribution
gulp.task("set-distribute-options", function() {
  options.compress      = true
  options.uncss         = true
  options.distribute    = true
})

// Distribute Task. Cleans then builds the dist directory, compresses JS files, UnCSS & compress the css files
//
// $ gulp distribute
//
//
gulp.task("distribute", function(cb) {

  // Distribute run task
  runSequence(["set-distribute-options", "clean"], "copy", ["pug-templates", "fonts", "images", "babel"], "compile-css", cb);
})

// ------------------------ //
//    DEPLOYMENT TASKS      //
// ------------------------ //

// Load the deployment tasks, contains: git-deploy, git-cp, git-add, git-commit, git-pull
require("./tasks/git-deploy")(gulp, plugins, options, runSequence)

// Automated Deployment Tasks
// To deploy to dec-ce.bitbucket.org/gef/(string) run the "deploy" task and pass a string of your choice (default: test)
//
// $ gulp deploy --env=review
//
//
gulp.task("deploy", function(cb) {

  // Set the environment if CLI parameter is set
  options.deployment.environment = argv.env !== undefined ? argv.env : options.deployment.environment

  if (options.deployment.environment !== undefined) {
    // Assign the new deployment location
    options.deployment.repository.deploymentLocation += options.deployment.environment
    // Run the Deployment
    return runSequence("distribute", "git-pull", cb);
  } else {
    console.log("Please supply a deployment environment. e.g. $ gulp deploy --env=GXF-420")
  }
})

// Default run task
gulp.task("default", function(cb) {
  gulp.start("watch", cb)
})
