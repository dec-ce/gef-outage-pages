"use strict"

module.exports = function(gulp, plugins, options, errorHandler, argv) {
  return function() {

    // jQuery
    gulp.src(options.paths.bower + "jquery/dist/" + (options.compress ? "jquery.min.js" : "jquery.js"))
      .pipe(plugins.rename("jquery.js"))
      .on("error", errorHandler)
      .pipe(gulp.dest(options.paths.dist.assets.js + "vendor"))

    // UIKit
    var uikitPaths
    if (options.compress) {
      uikitPaths = [options.paths.bower + "uikit/js/**/*.min.js"]
    } else {
      uikitPaths = [options.paths.bower + "uikit/js/**/*.js", "!" + options.paths.bower + "uikit/js/**/*.min.js"]
    }

    gulp.src(uikitPaths)
      .pipe(plugins.rename(function(path) {
        path.basename = path.basename.replace(".min", "")
      }))
      .on("error", errorHandler)
      .pipe(gulp.dest(options.paths.dist.assets.js + "vendor/uikit"))

    // GEF Fonts
    gulp.src(options.paths.gef + "src/assets/fonts/**/*.{eot,svg,ttf,woff,woff2,otf}")
      .pipe(gulp.dest(options.paths.dist.assets.fonts))

    // Font Awesome
    gulp.src(options.paths.bower + "uikit/fonts/**/*.{eot,svg,ttf,woff,woff2,otf}")
      .pipe(gulp.dest(options.paths.dist.assets.fonts))

    // RequireJS
    gulp.src(options.paths.bower + "requirejs/require.js")
      .pipe(plugins.if(options.compress, plugins.uglify()))
      .pipe(gulp.dest(options.paths.dist.assets.js + "vendor"))

    // Require CSS RequireJS plugin
    gulp.src(options.paths.bower + "require-css/" + (options.compress ? "css.min.js" : "css.js"))
      .pipe(plugins.rename("css.js"))
      .pipe(gulp.dest(options.paths.dist.assets.js + "vendor/requirejs-plugins/css"))

    // Config for Require
    gulp.src([options.paths.gef + "assets/js/config/config.js", options.paths.dev.assets.jsConfig + "siteConfig.js"])
      .pipe(gulp.dest(options.paths.dist.assets.js))

    // Placeholder support for IE9
    gulp.src(options.paths.bower + "jquery-placeholder/jquery.placeholder.js")
      .pipe(gulp.dest(options.paths.dist.assets.js + "vendor/jquery-placeholder"))

    if (argv.cms === "matrix") {
      // Images (svg, png, gif, jpg) found in src/assets/images
      gulp.src(options.paths.gef + "src/assets/images/**/*.{gif,jpg,svg,png}")
        .pipe(gulp.dest(options.paths.dist.assets.matrixImages))
        .pipe(gulp.dest(options.paths.dist.assets.images))
        .on("error", errorHandler)
    } else {
      // Images (svg, png, gif, jpg) found in src/assets/images
      gulp.src(options.paths.gef + "src/assets/images/**/*.{gif,jpg,svg,png}")
        .pipe(gulp.dest(options.paths.dist.assets.images))
        .on("error", errorHandler)
    }

  // Angular for Search SPA
    gulp.src(options.paths.bower + "angular/" + (options.compress ? "angular.min.js" : "angular.js"))
      .pipe(plugins.rename("angular.js"))
      .pipe(gulp.dest(options.paths.dist.assets.js + "vendor/angular"))

    gulp.src(options.paths.bower + "angular-sanitize/" + (options.compress ? "angular-sanitize.min.js" : "angular-sanitize.js"))
      .pipe(plugins.rename("angular-sanitize.js"))
      .pipe(gulp.dest(options.paths.dist.assets.js + "vendor/angular-sanitize"))

    gulp.src(options.paths.bower + "angular-utils-pagination/dirPagination.js")
      .pipe(gulp.dest(options.paths.dist.assets.js + "vendor/angular-utils-pagination"))

    // annyang voice recognititon
    gulp.src(options.paths.bower + "annyang/" + (options.compress ? "annyang.min.js" : "annyang.js"))
      .pipe(plugins.rename("annyang.js"))
      .pipe(gulp.dest(options.paths.dist.assets.js + "vendor/annyang"))

  }
}
