"use strict"

module.exports = function(gulp, plugins, options, errorHandler, pug) {

  return function() {

    return gulp.src(options.paths.dev.templates)

      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      // use gulp-cached to only run pug on templates that have changed.
      .pipe(plugins.cached('pug-templates'))

      // pug
      .pipe(plugins.pug({
        pretty: true,
        data: {
          revision: options.revision,
          configPath: "../js/"
        }
      }))

      .pipe(gulp.dest(options.paths.dist.root))


  }
}
