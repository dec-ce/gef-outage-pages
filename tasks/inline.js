"use strict"

module.exports = function(gulp, plugins, options, errorHandler, argv) {
  return function() {

    return gulp.src([options.paths.dist.html])
      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      .pipe(plugins.inline())

      .pipe(gulp.dest(options.paths.dist.root))

  }
}
